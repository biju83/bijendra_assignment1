from django.shortcuts import render

# Create your views here.

from django.views.generic import View
from django.http import HttpResponse,QueryDict
from django.core.exceptions import ObjectDoesNotExist



class Test(View):
    def get(self, request):

        return HttpResponse("2    +    2  =  4")
class TestSum(View):

    def get(self, request):
        try:
            p1 = request.GET['p1']
            p2 = request.GET['p2']
            result = int(p1) + int(p2)
            result1 = int(p1) - int(p2)
            result3 = int(p1) * int(p2)
            result4 = float(p1)/float(p2)
            return HttpResponse('sum is : ' + str(result) + '  \nsubstraction is :  '+ str(result1) + '  \nproduct is :   ' +
            str(result3) + '  \ndivision is:   ' + str(result4), status=200)
        except Exception as exception:
            print str(exception)
            return HttpResponse("attribute error", status=500)

from internship.models import Student
class Studentdata(View):

    def get(self,request):
        result=[]
        students=Student.objects.all()
        for student in students:
         res={}
         res['roll_number']=student.roll_number
         res['first_name']=student.first_name
         res['last_name']=student.last_name
         res['email']=student.email
         res['address']=student.address
         res['college']=student.college
         result.append(res)
        return HttpResponse(result,status = 200)
class Post(View):
    def post(self,request):
        roll_number=request.POST["roll_number"]
        first_name=request.POST["first_name"]
        last_name=request.POST["last_name"]
        email=request.POST["email"]
        address=request.POST["address"]
        college=request.POST["college"]
        student_info=Student(roll_number=roll_number,first_name=first_name,
        last_name=last_name,email=email,
        address=address,college=college)
        student_info.save()
        return HttpResponse(request)
class Deletedata(View):
    def delete(self,request,id):

        try:
            Student.objects.get(id=id).delete()
        #print(roll_number)
            return HttpResponse('suceessful')
        except ObjectDoesNotExist:
            return HttpResponse('record not found')
class Updatedata(View):
    def put(self,request,id):
     try:
        data =QueryDict(request.body)
        update_data=Student.objects.get(id=id)

        if data.get("first_name"):
            update_data.first_name=data.get("first_name")
        if data.get("last_name"):
            update_data.last_name=data.get("last_name")
        if data.get("email"):
            update_data.email=data.get("email")
        if data.get("address"):
            update_data.address=data.get("address")
        if data.get("college"):
            update_data.college=data.get("college")
        update_data.save()
        return HttpResponse(request)

     except ObjectDoesNotExist:
        return HttpResponse("Record Not Found")
