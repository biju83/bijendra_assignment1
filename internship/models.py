# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Student(models.Model):
   roll_number= models.CharField(max_length=255,default='null')
   first_name= models.CharField(max_length = 255,default='null')
   last_name = models.CharField(max_length = 255,default='null')
   email = models.CharField(max_length = 255,default='null')
   address=models.CharField(max_length = 255,default='null')
   college=models.CharField(max_length = 255,default='null')
