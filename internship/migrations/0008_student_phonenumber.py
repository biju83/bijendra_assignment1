# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-07-10 08:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('internship', '0007_remove_student_phonenumber'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='phonenumber',
            field=models.CharField(default='null', max_length=255),
        ),
    ]
