
from django.conf.urls import url
from django.contrib import admin
from internship.views import Test
from internship.views import TestSum
from internship.views import Studentdata
from internship.views import Post
from internship.views import Deletedata
from internship.views import Updatedata

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^hello/',Test.as_view(),name='home' ),
    url(r'^c/', TestSum.as_view(), name='h'),
    url(r'^get/',Studentdata.as_view(),name='biju'),
    url(r'^post/',Post.as_view(),name='b'),
    url(r'^del/(?P<id>\d+)$',Deletedata.as_view(),name='del'),
    url(r'^update/(?P<id>\d+)$',Updatedata.as_view(),name='u'),
    #r'^product/(?P<product_slug>  [-\w]+)/$'


]
